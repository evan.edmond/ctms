# cTMS

### Can altering TMS pulse width identify properties of vulnerable neurons in motor neuron disease?


This is a repository containing further background and analysis code from poster presentations of this project (2019 Brainbox Initiative conference).

Evan C. Edmond 2019.

![poster image](/poster/2019_brainbox_poster.png?raw=true)
