#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  9 14:34:18 2019

@author: evan

Various utilities for analysing TMS data, made into functions for easy code re-use
"""
import os
import csv
import glob
import re
import numpy as np
from pymatreader import read_mat
from matplotlib import pyplot as plt
import seaborn as sns


def unpackFrames(matPath):
    """
    Take path of .mat file exported from Signal, and return EMG data and Signal 
    states associated with each frame (sweep).
    """
    
    # Imported function from pymatreader module
    fileDict = read_mat(matPath)
        
    # Select wave_data struct from mat and get emg data and state value (as set in Signal configuration)
    for key in fileDict.keys():
        if 'wave_data' in key:
            wave_data = fileDict[key]
            emgData = wave_data['values']
            stateData = wave_data['frameinfo']['state']
            samplingInterval = wave_data['interval']
    return emgData, stateData, samplingInterval

def sigmoid(x, a, b, c):
    """
    Define sigmoid function used to fit curves (Boltzmann function)
    """
    return (a / (1.0 + np.exp((b - x)/c)))

def readStimSettings(filepath):
    """
    Open .csv file containing stim intensity settings associated with frame
    states, read into dict and return.
    """
    
    file = csv.DictReader(open(filepath))
    stimSettings = []
    
    for row in file:
        stimSettings.append(row)
    
    return stimSettings

def getPathsWithFiles(wildcardpath, patterns):
    """
    Returns all possible paths from wildcardpath where at least one file matching
    each pattern is present. Uses glob function for matching.
    
    example wildcardpath = '/path/of/interest/sub*/ses*/modality'
    example pattern = '*keyword*.extension'
    """
    
    # Get all possible paths from wildcardpath
    possible_paths = [x for x in glob.glob(wildcardpath)]
    
    # Get those paths where at least one file is present matching all patterns
    patterns_present = [sdp for sdp in possible_paths if all([glob.glob(os.path.join(sdp,p)) for p in patterns])]
            
    return patterns_present

def plotEachSweep(mep_sweep, outpath):
    
    plt.figure('persweep')
    plt.plot(mep_sweep)
    plt.hlines((mep_sweep.min(), mep_sweep.max()), xmin=0, xmax=len(mep_sweep), linestyles='dashed')
    
    plt.ylim(min([-2, mep_sweep.min()-0.1]), max([2, mep_sweep.max()+0.1]))
    plt.savefig(outpath)
    plt.close('persweep')
    
    return

def plotEachBlock(means, stdev, stimSequence, meps, indices, curvex, curvey, outpath):
    
    fig, axs = plt.subplots(nrows=1, ncols=1, dpi=300, num='perblock')
#    axs[0].plot(means.index, means['meps'], 'bo', markersize=5)
#    axs[0].errorbar(means.index, means['meps'], yerr = stdev['meps'], ls='none')
#    axs[0].plot(curvex, curvey, label='fit')
#    axs[0].set_xlabel('Stim intensity / %')
#    axs[0].set_ylabel('MEP amplitude/mV')
#    axs[0].set_title('Fitted I/O curve')
    
    axs.plot(stimSequence, meps, '+', markersize=4, label='MEP amplitude scatter')
    axs.plot(curvex, curvey, label='Fitted curve')
    axs.plot(means.index, means['meps'], 'ko', markersize=5)
    axs.errorbar(means.index, means['meps'], yerr = stdev['meps'], ls='none', label='Standard deviation')
    axs.set_xlim(60, 150)
    axs.set_xlabel('% motor threshold')
    axs.set_ylabel('MEP amplitude/mV')
    axs.set_title('Example recruitment curve')
    axs.legend()
    plt.savefig(outpath)
    
    plt.close('perblock')
    return

def groupIOplots(subj, ses, curvepars, outpath):
    
        
    fig, ax = plt.subplots(num='group', nrows=1, ncols=1, dpi=300)
    
    for key, pars in curvepars.items():
        
        # Use regex to search key for each set of curve parameters to label the curve
        curvelabel = re.findall('.hem_\d+', key)
        
        # Generate curve
        x = np.linspace(0, 200, 400)
        y = sigmoid(x, *pars)
        
        ax.plot(x, y, label=curvelabel[0])
        
        ax.set_xlabel('% motor threshold')
        ax.set_ylabel('MEP amplitude/mV')
        ax.set_xlim(50,150)
        ax.set_title('I/O curve at each pulse width')
        ax.legend()
        
    plt.savefig(outpath)
    plt.close('group')
    
    return